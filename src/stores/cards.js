import { defineStore } from 'pinia';

export const useCardsStore = defineStore('cards', {
    state: () => ({
        cards: [
            {
                id: 1,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 2,
                type: 'cat',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 3,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 4,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 5,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 6,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
            {
                id: 7,
                type: 'dog',
                img: '/src/assets/images/katrine.png',
                name: 'Katrine',
            },
        ],
    }),
});